package com.example.springboot.service;

import com.example.springboot.entity.Buku;
import com.example.springboot.repository.BukuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BukuService {
   @Autowired
   private BukuRepository repository;

    public Buku saveBuku(Buku buku){
       return repository.save(buku);
   }

    public List<Buku> getBukus() {
       return repository.findAll();
    }

    public String deleteBuku(int id) {
       repository.deleteById(id);
       return "Buku Removed";
    }

    public Optional<Buku> findById(int id) {
       return repository.findById(id);
    }

    public Buku updateBuku(Buku buku) {
        Buku existingBuku = (Buku) repository.findById(buku.getIdBuku()).orElse(null);
        existingBuku.setJudulBuku(buku.getJudulBuku());
        existingBuku.setPengarang(buku.getPengarang());
        existingBuku.setPenerbit(buku.getPenerbit());
        existingBuku.setTahunTerbit(buku.getTahunTerbit());
        return (Buku) repository.save(existingBuku);
    }
}
