package com.example.springboot.service;

import com.example.springboot.entity.Buku;
import com.example.springboot.entity.Member;
import com.example.springboot.repository.BukuRepository;
import com.example.springboot.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService {
    @Autowired
    private MemberRepository repository;

    public Member saveMember(Member member) {
        return repository.save(member);
    }

    public List<Member> getMembers() {
        return repository.findAll();
    }

    public String deleteMember(int id) {
        repository.deleteById(id);
        return "Member Removed";
    }

    public Member updateMember(Member member) {
        Member existingMember = (Member) repository.findById(member.getIdMember()).orElse(null);
        existingMember.setNama(member.getNama());
        existingMember.setAlamat(member.getAlamat());
        existingMember.setNoHp(member.getNoHp());
        return (Member) repository.save(existingMember);
    }
}
