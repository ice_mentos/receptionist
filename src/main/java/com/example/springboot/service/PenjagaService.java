package com.example.springboot.service;

import com.example.springboot.entity.Penjaga;
import com.example.springboot.repository.PenjagaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenjagaService {
    @Autowired
    private PenjagaRepository repository;

    public List<Penjaga> getPenjaga() {
        return repository.findAll();
    }

    public Penjaga getNamaPenjaga(String username){
        return repository.findByUsername(username);
    }
}
