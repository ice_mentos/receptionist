//package com.example.springboot.entity;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Table(name = "Peminjam")
//public class Peminjaman {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(nullable = false, length = 20)
//    private Integer idPeminjaman;
//
//    @Column(nullable = false, length = 20)
//    private String tanggalPinjam;
//
//    @Column(nullable = false, length = 20)
//    private String tanggalBalik;
//}
