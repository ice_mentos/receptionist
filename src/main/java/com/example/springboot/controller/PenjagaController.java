package com.example.springboot.controller;

import com.example.springboot.entity.Member;
import com.example.springboot.entity.Penjaga;
import com.example.springboot.service.BukuService;
import com.example.springboot.service.PenjagaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PenjagaController {
    @Autowired
    private PenjagaService penjagaService;

    //getPenjaga
    @GetMapping("/penjaga")
    public List<Penjaga> findAllPenjaga(){
        return penjagaService.getPenjaga();
    }

    @GetMapping("/penjaga/{username}")
    public Penjaga findByUsername(@PathVariable String username){
        return penjagaService.getNamaPenjaga(username);
    }

}
