package com.example.springboot.controller;

import com.example.springboot.entity.Buku;
import com.example.springboot.entity.Member;
import com.example.springboot.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    //addMember
    @PostMapping("/addMember")
    public Member addMember(@RequestBody Member member){
        return memberService.saveMember(member);
    }

    //getMember
    @GetMapping("/member")
    public List<Member> findAllMembers(){
        return memberService.getMembers();
    }

    //deleteMember
    @DeleteMapping("/deleteMember/{id}")
    public String deleteMember(@PathVariable int id){
        return memberService.deleteMember(id);
    }

    //update
    @PutMapping("/updatee/{id}")
    public Member updateMember(@RequestBody Member member, @PathVariable int id){
        member.setId(id);
        return  memberService.updateMember(member);
    }

//    //update
//    @PutMapping("/update/{id}")
//    public Member updateMember(@RequestBody Member member, @PathVariable int id){
//        member.setId(id);
//        return memberService.updateMember(member);
//    }
}
