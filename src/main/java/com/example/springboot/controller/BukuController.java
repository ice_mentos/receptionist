package com.example.springboot.controller;

import com.example.springboot.entity.Buku;
import com.example.springboot.service.BukuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*" )
@RestController
public class BukuController {
    @Autowired
    private BukuService bukuservice;

    //Nambah
    @PostMapping("/addBuku")
    public Buku addBuku(@RequestBody Buku buku){
        return bukuservice.saveBuku(buku);
    }

    //get
    @GetMapping("/buku")
    public List <Buku> findAllBukus(){
        return bukuservice.getBukus();
    }

    //delete
    @DeleteMapping("/delete/{id}")
    public String deleteBuku(@PathVariable int id){
        return bukuservice.deleteBuku(id);
    }

    //update
    @PutMapping("/update/{id}")
    public Buku updateBuku(@RequestBody Buku buku, @PathVariable int id){
        buku.setId(id);
        return bukuservice.updateBuku(buku);
    }

    //searchById
    @GetMapping("/buku/{id}")
    public Optional<Buku> findById(@PathVariable int id){
        return bukuservice.findById(id);
    }
}
