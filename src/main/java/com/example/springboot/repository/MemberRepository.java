package com.example.springboot.repository;

import com.example.springboot.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Integer> {
//    Member findByName(String name);
}
