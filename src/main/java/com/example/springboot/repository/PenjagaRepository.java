package com.example.springboot.repository;

import com.example.springboot.entity.Penjaga;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PenjagaRepository extends JpaRepository<Penjaga, Integer> {
    Penjaga findByUsername(String username);
}
