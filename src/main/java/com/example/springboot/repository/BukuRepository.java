package com.example.springboot.repository;

import com.example.springboot.entity.Buku;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BukuRepository extends JpaRepository<Buku, Integer> {
//    Buku save(String judul);
}
